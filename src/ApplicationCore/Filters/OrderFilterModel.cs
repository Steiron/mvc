using System;

namespace ApplicationCore.Filters
{
    public class OrderFilterModel
    {
        public string[] Number { get; set; }
        public DateTime Date { get; set; }
        public int[] ProviderId { get; set; }
    }
}