namespace ApplicationCore.Filters
{
    public class OrderItemFilterModel
    {
        public string Name { get; set; }
        public string Unit { get; set; }
    }
}