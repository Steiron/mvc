using System;
using System.Collections.Generic;

namespace ApplicationCore.Enities
{
    public class Order : BaseEntity
    {
        public string Number { get; private set; }
        public DateTime Date { get; private set; }
        public int ProviderId { get; private set; }

        public Provider Provider { get; private set; }
        public ICollection<OrderItem> OrderItems { get; private set; }

        private Order()
        {
        }

        public Order(string number, DateTime date, int providerId)
        {
            Number = number;
            Date = date;
            ProviderId = providerId;
        }

        public void Update(string number, DateTime date, int providerId)
        {
            Number = number;
            Date = date;
            ProviderId = providerId;
        }
    }
}