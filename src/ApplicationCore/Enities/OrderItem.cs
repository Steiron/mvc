namespace ApplicationCore.Enities
{
    public class OrderItem : BaseEntity
    {
        public string Name { get; private set; }
        public double Quantity { get; private set; }
        public string Unit { get; private set; }
        public int OrderId { get; private set; }
        public Order Order { get; private set; }

        private OrderItem()
        {
        }

        public OrderItem(int orderId, string name, double quantity, string unit)
        {
            OrderId = orderId;
            Name = name;
            Quantity = quantity;
            Unit = unit;
        }

        public void Update(string name, double quantity, string unit)
        {
            Name = name;
            Quantity = quantity;
            Unit = unit;
        }
    }
}