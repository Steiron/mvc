namespace ApplicationCore.Enities
{
    public class Provider : BaseEntity
    {
        public string Name { get; private set; }

        private Provider()
        {
        }

        public Provider(string name) => Name = name;

    }
}