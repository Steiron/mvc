using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Enities;
using ApplicationCore.Filters;

namespace ApplicationCore.Interfaces.Services
{
    public interface IOrderService
    {
        Task<IReadOnlyCollection<Order>> GetAll();

        Task<IReadOnlyCollection<Order>> GetAllWithFilter(OrderFilterModel orderFilterModel);

        Task<Order> GetById(int id);
        Task<Order> GetByIdWithOrderItems(int id);
        Task<Order> Create(Order order);
        Task<Order> Update(int id, Order order);
        Task<Order> Delete(int id);
    }
}