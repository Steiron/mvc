using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Enities;

namespace ApplicationCore.Interfaces.Services
{
    public interface IProviderService
    {
        Task<IReadOnlyCollection<Provider>> GetAll();

        Task<Provider> GetById(int id);
    }
}