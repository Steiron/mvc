using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Enities;

namespace ApplicationCore.Interfaces.Services
{
    public interface IOrderItemService
    {
        Task<IReadOnlyCollection<OrderItem>> GetAllByOrderId(int orderId);
        Task<IReadOnlyCollection<OrderItem>> GetAllWithFilterByOrderId(int orderId);
        Task<OrderItem> GetById(int id);
        Task<OrderItem> GetByOrderId(int orderId);
        Task<IReadOnlyCollection<OrderItem>> Create(IReadOnlyCollection<OrderItem> orderItems);
        Task<OrderItem> Update(int id, OrderItem orderItem);
        Task<OrderItem> Delete(int id);
    }
}