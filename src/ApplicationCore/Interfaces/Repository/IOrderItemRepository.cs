using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Enities;

namespace ApplicationCore.Interfaces.Repository
{
    public interface IOrderItemRepository : IGenericRepository<OrderItem>
    {
        Task<IReadOnlyCollection<OrderItem>> GetAllByOrderId(int orderId);
        Task<OrderItem> GetByOrderId(int orderId);
    }
}