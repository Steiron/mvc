using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Enities;
using ApplicationCore.Filters;

namespace ApplicationCore.Interfaces.Repository
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
        Task<IReadOnlyCollection<Order>> GetAllWithFilter(OrderFilterModel orderFilterModel);
        Task<Order> GetByIdWithOrderItems(int orderId);
    }
}