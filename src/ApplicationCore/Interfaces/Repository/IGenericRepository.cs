using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Enities;

namespace ApplicationCore.Interfaces.Repository
{
    public interface IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        Task<TEntity> GetByIdAsync(int id);
        Task<IReadOnlyCollection<TEntity>> GetAllAsync();
        Task<TEntity> AddAsync(TEntity entity);
        Task<IReadOnlyCollection<TEntity> > AddRangeAsync(IReadOnlyCollection<TEntity> entity);
        TEntity Update(TEntity entity);
        TEntity Delete(TEntity entity);
    }
}