using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Enities;
using ApplicationCore.Filters;
using ApplicationCore.Interfaces.Repository;
using ApplicationCore.Interfaces.Services;

namespace ApplicationCore.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _repository;

        public OrderService(IOrderRepository repository)
        {
            _repository = repository;
        }


        public async Task<IReadOnlyCollection<Order>> GetAll()
        {
            return await _repository.GetAllAsync();
        }

        public async Task<IReadOnlyCollection<Order>> GetAllWithFilter(OrderFilterModel orderFilterModel)
        {
            return await _repository.GetAllWithFilter(orderFilterModel);
        }

        public async Task<Order> GetById(int id)
        {
            return await _repository.GetByIdAsync(id);
        }

        public async Task<Order> GetByIdWithOrderItems(int id)
        {
            return await _repository.GetByIdWithOrderItems(id);
        }

        public async Task<Order> Create(Order order)
        {
            Order newOrder = new Order(order.Number, order.Date, order.ProviderId);
            return await _repository.AddAsync(newOrder);
        }

        public async Task<Order> Update(int id, Order order)
        {
            Order curOrder = await GetById(id);
            if (curOrder == null) return null;
            curOrder.Update(order.Number, order.Date, order.ProviderId);
            _repository.Update(curOrder);
            return curOrder;
        }

        public async Task<Order> Delete(int id)
        {
            Order curOrder = await GetById(id);
            if (curOrder == null) return null;
            return _repository.Delete(curOrder);
        }
    }
}