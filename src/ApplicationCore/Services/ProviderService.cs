using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Enities;
using ApplicationCore.Interfaces.Repository;
using ApplicationCore.Interfaces.Services;

namespace ApplicationCore.Services
{
    public class ProviderService : IProviderService
    {
        private readonly IGenericRepository<Provider> _repository;

        public ProviderService(IGenericRepository<Provider> repository)
        {
            _repository = repository;
        }

        public async Task<IReadOnlyCollection<Provider>> GetAll()
        {
            return await _repository.GetAllAsync();
        }

        public async Task<Provider> GetById(int id)
        {
            return await _repository.GetByIdAsync(id);
        }
    }
}