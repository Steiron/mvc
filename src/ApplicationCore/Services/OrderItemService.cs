using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Enities;
using ApplicationCore.Interfaces.Repository;
using ApplicationCore.Interfaces.Services;

namespace ApplicationCore.Services
{
    public class OrderItemService : IOrderItemService
    {
        private readonly IOrderItemRepository _repository;

        public OrderItemService(IOrderItemRepository repository)
        {
            _repository = repository;
        }

        public async Task<IReadOnlyCollection<OrderItem>> GetAllByOrderId(int orderId)
        {
            return await _repository.GetAllByOrderId(orderId);
        }

        public Task<IReadOnlyCollection<OrderItem>> GetAllWithFilterByOrderId(int orderId)
        {
            throw new System.NotImplementedException();
        }

        public async Task<OrderItem> GetById(int id)
        {
            return await _repository.GetByIdAsync(id);
        }

        public Task<OrderItem> GetByOrderId(int orderId)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IReadOnlyCollection<OrderItem>> Create(IReadOnlyCollection<OrderItem> orderItems)
        {
            return await _repository.AddRangeAsync(orderItems);
        }


        public Task<OrderItem> Update(int id, OrderItem orderItem)
        {
            throw new System.NotImplementedException();
        }

        public Task<OrderItem> Delete(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}