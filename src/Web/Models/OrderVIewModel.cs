using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ApplicationCore.Enities;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Web.Models
{
    public class OrderViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Number { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public int ProviderId { get; set; }
        public Provider Provider { get; set; }
        public SelectList Providers { get; set; }
        public IReadOnlyCollection<OrderItemViewModel> OrderItems { get; set; } = new List<OrderItemViewModel>();
        public bool IsEdit { get; private set; }

        public OrderViewModel()
        {
        }

        public OrderViewModel(IReadOnlyCollection<Provider> providers)
        {
            Providers = new SelectList(providers, "Id", "Name");
        }

        public OrderViewModel(Order order)
        {
            Id = order.Id;
            Number = order.Number;
            Date = order.Date;
            ProviderId = order.ProviderId;
            Provider = order.Provider;
            OrderItems = order.OrderItems.Select(o => new OrderItemViewModel(o)).ToList();
        }

        public void SetEdit() => IsEdit = true;

        public Order ToOrder()
        {
            return new Order(Number, Date, ProviderId);
        }

        public IReadOnlyCollection<OrderItem> ToOrderItemList(int orderId)
        {
            return OrderItems.Select(o => o.ToOrderItem(orderId)).ToList();
        }
    }
}