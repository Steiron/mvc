using ApplicationCore.Enities;

namespace Web.Models
{
    public class OrderItemViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Quantity { get; set; }
        public string Unit { get; set; }
        public int OrderId { get; set; }

        public OrderItemViewModel()
        {
        }

        public OrderItemViewModel(OrderItem orderItem)
        {
            Id = orderItem.Id;
            Name = orderItem.Name;
            Quantity = orderItem.Quantity;
            Unit = orderItem.Unit;
            OrderId = orderItem.OrderId;
        }

        public OrderItem ToOrderItem(int orderId)
        {
            return new OrderItem(orderId, Name, Quantity, Unit);
        }
    }
}