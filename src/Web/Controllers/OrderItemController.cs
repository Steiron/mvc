﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Enities;
using ApplicationCore.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class OrderItemController : Controller
    {
        private readonly IOrderItemService _orderItemService;


        public OrderItemController(IOrderItemService orderItemService)
        {
            _orderItemService = orderItemService;
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            return PartialView();
        }

    }
}