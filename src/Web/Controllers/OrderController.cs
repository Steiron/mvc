﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Enities;
using ApplicationCore.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IOrderItemService _orderItemService;
        private readonly IProviderService _providerService;

        public OrderController(IOrderService orderService,
            IProviderService providerService,
            IOrderItemService orderItemService)
        {
            _orderService = orderService;
            _providerService = providerService;
            _orderItemService = orderItemService;
        }

        public async Task<IActionResult> Index(int id)
        {
            Order order = await _orderService.GetByIdWithOrderItems(id);
            OrderViewModel viewModel = new OrderViewModel(order);
            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            IReadOnlyCollection<Provider> providers = await _providerService.GetAll();
            OrderViewModel orderViewModel = new OrderViewModel(providers);
            return View(orderViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(OrderViewModel orderViewModel)
        {
            Order order = orderViewModel.ToOrder();
            order = await _orderService.Create(order);
            var orderItems = orderViewModel.ToOrderItemList(order.Id);
            await _orderItemService.Create(orderItems);

            return RedirectToAction("Index", new {orderId = order.Id});
        }
    }
}