using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Enities;

namespace Infrastructure.Data
{
    public class DataSeed
    {

        public static async Task SeedAsync(DataContext context)
        {
            if (!context.Providers.Any())
            {
                await context.Providers.AddRangeAsync(SeedProviders());
                await context.SaveChangesAsync();
            }

            if (!context.Orders.Any())
            {
                await context.Orders.AddRangeAsync(SeedOrders());
                await context.SaveChangesAsync();
            }

            if (!context.OrderItems.Any())
            {
                await context.OrderItems.AddRangeAsync(SeedOrderItems());
                await context.SaveChangesAsync();
            }
        }


        public static IEnumerable<Provider> SeedProviders()
        {
            return new[]
            {
                new Provider("Поставщик #1"),
                new Provider("Поставщик #2"),
                new Provider("Поставщик #3"),
                new Provider("Поставщик #4"),
                new Provider("Поставщик #5"),
            };
        }

        public static IEnumerable<Order> SeedOrders()
        {
            return new[]
            {
                new Order("Заказ 101", DateTime.Now, 1),
                new Order("Заказ 102", DateTime.Now.AddDays(-2), 2),
                new Order("Заказ 104", DateTime.Now.AddDays(-2), 3),
                new Order("Заказ 105", DateTime.Now.AddDays(-2), 1),
                new Order("Заказ 106", DateTime.Now.AddDays(-4), 3),
                new Order("Заказ 107", DateTime.Now.AddDays(-4), 4),
            };
        }

        public static IEnumerable<OrderItem> SeedOrderItems()
        {
            return new[]
            {
                new OrderItem(1, "Товар 1", 104.99, "2"),
                new OrderItem(1, "Товар 2", 14.99, "1"),
                new OrderItem(2, "Товар 24", 145.99, "1"),
                new OrderItem(3, "Товар 1", 104.99, "3"),
            };
        }
    }
}