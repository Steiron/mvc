using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Enities;
using ApplicationCore.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data.Repository
{
    public class OrderItemRepository : GenericRepository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(DataContext dataContext) : base(dataContext)
        {
        }

        public async Task<IReadOnlyCollection<OrderItem>> GetAllByOrderId(int orderId)
        {
            return await DbSet.Where(o => o.OrderId == orderId)
                .Include(o => o.Order).ToListAsync();
        }

        public async Task<OrderItem> GetByOrderId(int orderId)
        {
            return await DbSet.FirstOrDefaultAsync(o => o.OrderId == orderId);
        }

        public Task<OrderItem> AddOrderItem(int orderId, OrderItem orderItem)
        {
            throw new System.NotImplementedException();
        }
    }
}