using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Enities;
using ApplicationCore.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly DataContext DataContext;
        protected readonly DbSet<TEntity> DbSet;


        public GenericRepository(DataContext dataContext)
        {
            DataContext = dataContext;
            DbSet = DataContext.Set<TEntity>();
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public async Task<IReadOnlyCollection<TEntity>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            await DbSet.AddAsync(entity);
            Save();
            return entity;
        }

        public async Task<IReadOnlyCollection<TEntity> > AddRangeAsync(IReadOnlyCollection<TEntity> entities)
        {
            await DbSet.AddRangeAsync(entities);
            Save();
            return entities;
        }

        public TEntity Update(TEntity entity)
        {
            DbSet.Attach(entity).State = EntityState.Modified;
            Save();
            return entity;
            
        }

        public TEntity Delete(TEntity entity)
        {
            DbSet.Remove(entity);
            Save();
            return entity;
        }

        protected void Save()
        {
            DataContext.SaveChanges();
        }
    }
}