using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Enities;
using ApplicationCore.Filters;
using ApplicationCore.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data.Repository
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(DataContext dataContext) : base(dataContext)
        {
        }

        public Task<IReadOnlyCollection<Order>> GetAllWithFilter(OrderFilterModel orderFilterModel)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Order> GetByIdWithOrderItems(int orderId)
        {
            return await DbSet.Where(o => o.Id == orderId)
                .Include(o => o.OrderItems)
                .Include(o => o.Provider)
                .FirstOrDefaultAsync();
        }
    }
}