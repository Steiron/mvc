using ApplicationCore.Enities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.ModelMapping
{
    public class OrderItemMap : IEntityTypeConfiguration<OrderItem>
    {
        public void Configure(EntityTypeBuilder<OrderItem> builder)
        {
            builder.ToTable("OrderItem");
            builder.HasKey(p => p.Id);
            builder.HasOne(p => p.Order)
                .WithMany(p => p.OrderItems)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Property(p => p.Quantity).HasColumnType("decimal(18,3)");
        }
    }
}